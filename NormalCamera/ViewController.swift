//
//  ViewController.swift
//  NormalCamera
//
//  Created by SHOKI TAKEDA on 3/20/16.
//  Copyright © 2016 morningcamera.com. All rights reserved.
//

import UIKit
import Social
import Foundation
import CoreImage

extension String {
    
    /// String -> NSString に変換する
    func to_ns() -> NSString {
        return (self as NSString)
    }
    
    func substringFromIndex(index: Int) -> String {
        return to_ns().substringFromIndex(index)
    }
    
    func substringToIndex(index: Int) -> String {
        return to_ns().substringToIndex(index)
    }
    
    func substringWithRange(range: NSRange) -> String {
        return to_ns().substringWithRange(range)
    }
    
    var lastPathComponent: String {
        return to_ns().lastPathComponent
    }
    
    var pathExtension: String {
        return to_ns().pathExtension
    }
    
    var stringByDeletingLastPathComponent: String {
        return to_ns().stringByDeletingLastPathComponent
    }
    
    var stringByDeletingPathExtension: String {
        return to_ns().stringByDeletingPathExtension
    }
    
    var pathComponents: [String] {
        return to_ns().pathComponents
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
        return to_ns().stringByAppendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        return to_ns().stringByAppendingPathExtension(ext)
    }
    
}

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NADViewDelegate {
    var upCameraBool:Bool = true
    var globalImgWidth:CGFloat = 0
    var globalImgHeight:CGFloat = 0
    var globalImg:UIImage?
    var globalRatio:CGFloat = 0
    var postImg:UIImage?
    var originalImg:UIImage?
    
    var globalUnderLeftX:CGFloat = 0
    var globalUnderLeftY:CGFloat = 0
    var globalUnderWidth:CGFloat = 0
    var globalUnderHeight:CGFloat = 0
    var globalUnderRightX:CGFloat = 0
    var globalUnderRightY:CGFloat = 0
    var globalNoseX:CGFloat = 0
    var globalNoseY:CGFloat = 0
    var globalNoseWidth:CGFloat = 0
    var globalNoseHeight:CGFloat = 0
    
    var underLeftBool:Bool = false
    var underRightBool:Bool = false
    var noseBool:Bool = false
    
    var myViewFrame1: CGRect = CGRectMake(0, 0, 0, 0)
    var myViewFrame2: CGRect = CGRectMake(0, 0, 0, 0)
    var myViewFrame3: CGRect = CGRectMake(0, 0, 0, 0)
    
    var shipImageView : UIImageView!
    var myImageView1:UIImageView!
    var myImageView2:UIImageView!
    var myImageView3:UIImageView!
    var myImageView4:UIImageView!
    var myImageView5:UIImageView!
    var myImageView6:UIImageView!
    var myImageView7:UIImageView!
    var myImageView8:UIImageView!
    var myImageView9:UIImageView!
    
    private var nadView: NADView!
    var adTimer:NSTimer!
    
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var topImgHeight: NSLayoutConstraint!
    @IBOutlet weak var topImgWidth: NSLayoutConstraint!
    @IBOutlet weak var instagramBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var cameraImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
                          spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        twitterBtn.backgroundColor = UIColor.clearColor()
        twitterBtn.setImage(UIImage(named: "twitter-big.jpg"), forState: .Normal)
        twitterBtn.imageView!.contentMode = .ScaleAspectFit
        
        fbBtn.backgroundColor = UIColor.clearColor()
        fbBtn.setImage(UIImage(named: "facebook-big.jpg"), forState: .Normal)
        fbBtn.imageView!.contentMode = .ScaleAspectFit
        
        instagramBtn.backgroundColor = UIColor.clearColor()
        instagramBtn.setImage(UIImage(named: "instagram"), forState: .Normal)
        instagramBtn.imageView!.contentMode = .ScaleAspectFit
        
        saveBtn.backgroundColor = UIColor.clearColor()
        saveBtn.setImage(UIImage(named: "save-big"), forState: .Normal)
        saveBtn.imageView!.contentMode = .ScaleAspectFit
        
        cameraBtn.backgroundColor = UIColor.clearColor()
        cameraBtn.setImage(UIImage(named: "camera-big"), forState: .Normal)
        cameraBtn.imageView!.contentMode = .ScaleAspectFit
        
        selectBtn.backgroundColor = UIColor.clearColor()
        selectBtn.setImage(UIImage(named: "album"), forState: .Normal)
        selectBtn.imageView!.contentMode = .ScaleAspectFit
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if upCameraBool == false && globalImg != nil {
            if shipImageView != nil {
                shipImageView.removeFromSuperview()
            }
            if myImageView1 != nil {
                myImageView1.removeFromSuperview()
            }
            if myImageView2 != nil {
                myImageView2.removeFromSuperview()
            }
            let picName:String = "nkusakina.jpg"
            let uiImg:UIImage = UIImage(named:picName)!
            let data = UIImagePNGRepresentation(uiImg)
            self.shipImageView = UIImageView(image:UIImage(data:data!))
            globalRatio = globalImg!.size.width / globalImg!.size.height
            globalImgHeight = self.view.frame.height - 180
            self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
            self.view.addSubview(self.shipImageView)
            
            let myImage : UIImage = UIImage.ResizeÜIImage(globalImg!, width: self.view.frame.width, height: self.view.frame.height)
            let options : NSDictionary = NSDictionary(object: CIDetectorAccuracyHigh, forKey: CIDetectorAccuracy)
            let detector : CIDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options as! [String : AnyObject])
            let faces : NSArray = detector.featuresInImage(CIImage(image: myImage)!)
            var transform : CGAffineTransform = CGAffineTransformMakeScale(1, -1)
            transform = CGAffineTransformTranslate(transform, 0, -self.view.frame.height)
            var globalLeftEyeX:CGFloat = 0
            var globalLeftEyeY:CGFloat = 0
            var globalRightEyeX:CGFloat = 0
            var globalRightEyeY:CGFloat = 0
            var globalFaceMinX:CGFloat = 0
            var globalFaceMaxX:CGFloat = 0
            var globalFaceMinY:CGFloat = 0
            var globalFaceMaxY:CGFloat = 0
            var faceCounter:Int = 0
            for feature in faces {
                if faceCounter == 0 {
                    globalLeftEyeX = feature.leftEyePosition.x
                    globalLeftEyeY = feature.leftEyePosition.y
                    globalRightEyeX = feature.rightEyePosition.x
                    globalRightEyeY = feature.rightEyePosition.y
                    globalFaceMinX = feature.bounds.minX
                    globalFaceMaxX = feature.bounds.maxX
                    globalFaceMinY = feature.bounds.minY
                    globalFaceMaxY = feature.bounds.maxY
                }
                faceCounter++
            }
            
            self.shipImageView.image = globalImg
            let faceWidth = globalFaceMaxX-globalFaceMinX
            let radian = atan2(Double(-(globalLeftEyeY - globalRightEyeY)), Double(globalLeftEyeX - globalRightEyeX))
            var theta:CGFloat = CGFloat(radian/M_PI*180.0)
            if theta > 0 {
                theta = 180 - theta
            } else if theta == 0 {
                theta = 0
            } else {
                theta = -(theta + 180)
            }
            theta = -theta
            let angle:CGFloat = CGFloat((Double(theta) * M_PI) / 180.0)
            
            let myInputImage1 = CIImage(image: UIImage(named: "nflare")!)
            myImageView1 = UIImageView(frame: CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2 , self.view.frame.height/2 - globalImgHeight/2, 200, 200))
            myImageView1.image = UIImage(CIImage: myInputImage1!)
            myImageView1.transform = CGAffineTransformMakeRotation(angle)
            myImageView1.alpha = 0.1
            self.view.addSubview(myImageView1)
            self.view.bringSubviewToFront(myImageView1)
            
            globalUnderLeftX = globalLeftEyeX-faceWidth/8
            globalUnderLeftY = self.view.frame.height-(globalLeftEyeY-faceWidth/70)*(globalImgHeight/self.view.frame.height)-(self.view.frame.height/2 - globalImgHeight/2)
            globalUnderWidth = faceWidth/5
            globalUnderHeight = faceWidth/10
            
            let cropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
            UIGraphicsBeginImageContext(self.view.bounds.size)
            self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
            let overViewImg = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            let cropRef   = CGImageCreateWithImageInRect(overViewImg.CGImage, cropRect)
            let cropImage = UIImage(CGImage: cropRef!)
            
            originalImg = cropImage
            postImg = cropImage
            
            if shipImageView != nil {
                shipImageView.removeFromSuperview()
            }
            
            let ciImage:CIImage = CIImage(image:originalImg!)!
            let ciFilter:CIFilter = CIFilter(name: "CILinearToSRGBToneCurve")!
            ciFilter.setValue(ciImage, forKey: "inputImage")
            let ciContext:CIContext = CIContext(options: nil)
            let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
            let image2:UIImage? = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
            
//            let ciImage:CIImage = CIImage(image:originalImg!)!
//            let ciFilter:CIFilter = CIFilter(name: "CIColorClamp")!
//            ciFilter.setValue(ciImage, forKey: "inputImage")
//            ciFilter.setValue(CIVector(x: 0.9, y: 0.9, z: 0.9, w: 0.9), forKey: "inputMaxComponents")
//            ciFilter.setValue(CIVector(x: 0.2, y: 0.2, z: 0.2, w: 0.2), forKey: "inputMinComponents")
//            let ciContext:CIContext = CIContext(options: nil)
//            let cgimg:CGImageRef = ciContext.createCGImage(ciFilter.outputImage!, fromRect:ciFilter.outputImage!.extent)
//            let image2:UIImage = UIImage(CGImage: cgimg, scale: 1.0, orientation:UIImageOrientation.Up)
            
            self.shipImageView = UIImageView(image:image2)
            self.shipImageView.frame = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
            self.view.addSubview(self.shipImageView)

            let ncropRect  = CGRectMake((self.view.frame.width - globalImgHeight * globalRatio)/2, self.view.frame.height/2 - globalImgHeight/2, globalImgHeight * globalRatio, globalImgHeight)
            UIGraphicsBeginImageContext(self.view.bounds.size)
            self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
            let noverViewImg = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            let ncropRef   = CGImageCreateWithImageInRect(noverViewImg.CGImage, ncropRect)
            let ncropImage = UIImage(CGImage: ncropRef!)
            
            postImg = ncropImage
        }
        cameraImgView.hidden = false
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // タッチイベントを取得
        let touchEvent = touches.first!
        
        // ドラッグ前の座標, Swift 1.2 から
        let preDx = touchEvent.previousLocationInView(self.view).x
        let preDy = touchEvent.previousLocationInView(self.view).y
        
        var lag:CGFloat = 20
        
//        if preDx < globalUnderLeftX + globalUnderWidth && preDy > globalUnderLeftY - globalUnderHeight {
        if preDx > globalUnderLeftX - lag && preDx < globalUnderLeftX + globalUnderWidth + lag && preDy < globalUnderLeftY + lag && preDy > globalUnderLeftY - globalUnderHeight - lag {
            myViewFrame1 = myImageView1.frame
            underLeftBool = true
        }
        if underLeftBool == true {
            // ドラッグ後の座標
            let newDx = touchEvent.locationInView(self.view).x
            let newDy = touchEvent.locationInView(self.view).y
            // ドラッグした座標の移動距離
//            let dx = newDx - preDx
//            let dy = newDy - preDy
            // 移動分を反映させる
            myViewFrame1.origin.x = newDx
            myViewFrame1.origin.y = newDy
            let newViewFrame: CGRect = CGRectMake(newDx, newDy, globalUnderWidth, globalUnderHeight)
            globalUnderLeftX = myViewFrame1.origin.x
            globalUnderLeftY = myViewFrame1.origin.y
            
            myImageView1.frame = newViewFrame
            underLeftBool = false
            self.view.addSubview(myImageView1)
        }
        
        //        if preDx < globalUnderRightX + globalUnderWidth && preDy < globalUnderRightY && preDy > globalUnderRightY - globalUnderHeight{
        
        if preDx > globalUnderRightX - lag && preDx < globalUnderRightX + globalUnderWidth + lag && preDy < globalUnderRightY + lag && preDy > globalUnderRightY - globalUnderHeight - lag {
            myViewFrame2 = myImageView2.frame
            underRightBool = true
        }
        if underRightBool == true {
            // ドラッグ後の座標
            let newDx = touchEvent.locationInView(self.view).x
            let newDy = touchEvent.locationInView(self.view).y
            // ドラッグした座標の移動距離
//            let dx = newDx - preDx
//            let dy = newDy - preDy
            // 移動分を反映させる
            myViewFrame2.origin.x = newDx
            myViewFrame2.origin.y = newDy
            let newViewFrame: CGRect = CGRectMake(newDx, newDy, globalUnderWidth, globalUnderHeight)
            globalUnderRightX = newDx
            globalUnderRightY = newDy
            
            myImageView2.frame = newViewFrame
            underRightBool = false
            self.view.addSubview(myImageView2)
        }
        
//        if preDy < globalNoseY {
        if preDx > globalNoseX - lag && preDx < globalNoseX + globalNoseWidth + lag && preDy < globalNoseY + lag && preDy > globalNoseY - globalNoseHeight - lag {
            myViewFrame3 = myImageView9.frame
            noseBool = true
        }
        if noseBool == true {
            // ドラッグ後の座標
            let newDx = touchEvent.locationInView(self.view).x
            let newDy = touchEvent.locationInView(self.view).y
            // ドラッグした座標の移動距離
//            let dx = newDx - preDx
//            let dy = newDy - preDy
            // 移動分を反映させる
            myViewFrame3.origin.x = newDx
            myViewFrame3.origin.y = newDy
            let newViewFrame: CGRect = CGRectMake(newDx, newDy, globalNoseWidth, globalNoseHeight)
            globalNoseX = newDx
            globalNoseY = newDy
            
            myImageView9.frame = newViewFrame
            noseBool = false
            self.view.addSubview(myImageView9)
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if upCameraBool == true {
            let picker:UIImagePickerController = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(picker, animated: true, completion: nil)
        }
        upCameraBool = false
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        let stampViews = cameraImgView.subviews
        for stampView in stampViews {
            stampView.removeFromSuperview()
        }
        globalImg = image
        originalImg = image
        cameraImgView.hidden = false
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func twitter(sender: AnyObject) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Twitter?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            let title: String = "#MorningCamera"
            controller.setInitialText(title)
            controller.addImage(self.postImg)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func facebook(sender: AnyObject) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Facebok?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            let title: String = "#MorningCamera"
            controller.setInitialText(title)
            controller.addImage(self.postImg)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    var documentController:UIDocumentInteractionController?
    @IBAction func instagram(sender: AnyObject) {
        let imageData = UIImageJPEGRepresentation(postImg!, 1.0)
        let temporaryDirectory = NSTemporaryDirectory() as String
        let temporaryImagePath = temporaryDirectory.stringByAppendingPathComponent("postImg.igo")
        let boolValue = imageData!.writeToFile(temporaryImagePath, atomically: true)
        let fileURL = NSURL(fileURLWithPath: temporaryImagePath)
        documentController = UIDocumentInteractionController(URL: fileURL)
        documentController!.UTI = "com.instagram.exclusivegram"
        documentController!.presentOpenInMenuFromRect(
            self.view.frame,
            inView: self.view,
            animated: true
        )
    }
    
    @IBAction func reserve(sender: UIButton) {
        if postImg != nil {
            UIImageWriteToSavedPhotosAlbum(postImg!, self, nil, nil)
            let alertController = UIAlertController(title: "Complete", message: "Successfully Saved.", preferredStyle: .Alert)
            let otherAction = UIAlertAction(title: "OK", style: .Default) {
                action in self.dismissViewControllerAnimated(true, completion: nil)
            }
            alertController.addAction(otherAction)
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func takeAgain(sender: UIButton) {
        let picker:UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    @IBAction func selectImg(sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePickerController.allowsEditing = false
        imagePickerController.delegate = self
        self.presentViewController(imagePickerController,animated:true ,completion:nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
                          spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
                          spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}

